#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <sys/wait.h>
#include<sys/types.h>
#include<unistd.h>
// https://stackoverflow.com/questions/35471521/waitstatus-wexitstatusstatus-always-returns-0
// https://www.ibm.com/support/knowledgecenter/SSB23S_1.1.0.14/gtpc2/cpp_wait.html?view=embed#cpp_wait
typedef struct palabrasTDA {
  char *palabra;
  int cantidad;
  // struct palabrasTDA *siguiente;
}palabrasTDA;

char line[500];

palabrasTDA *crearPalabra(char *palabraTmp) {
	palabrasTDA *palabra = malloc(sizeof(palabrasTDA));
	palabra->palabra = palabraTmp;
	palabra->cantidad = 1;
	return palabra;
}

int generar(palabrasTDA **palabrasArray, int indexArray, char *palabraTmp) {
	int i = 0;
	if (palabrasArray[0] == NULL) {
		// printf("NO EXISTE: %s\n", palabraTmp);
		palabrasTDA *palabra =  crearPalabra(palabraTmp);
		palabrasArray[0] = palabra;
		// printf("%s\n", palabra->palabra);
		return 0;
	}
	palabrasTDA *palabra = palabrasArray[0];
	do {
		palabra = palabrasArray[i];
		if (palabra != NULL) {
			if (strcmp(palabra->palabra, palabraTmp) == 0) {
				// printf("SUMADO: %s\n", palabraTmp);
				palabra->cantidad++;
				return 0;
			}
		}
		i++;
	} while(palabra != NULL);
	palabrasArray[i -1] =  crearPalabra(palabraTmp);
	return 0;
}


int main(int argc, char **argv) {

	if (argc != 3) {
		printf("./prog \"String a analizar \" nombre_archivo");
		exit(1);
	}
	char *string_analizar = argv[1];
	char *nombre_archivo = argv[2];
	char *palabraTmp = malloc(sizeof(50));
	FILE *fp = fopen(nombre_archivo, "w" );
	int indexPalabraTmp = 0;
	int indexArray = 0;

	int espacios = 0;
	for (int i = 0; i < strlen(string_analizar); ++i) {
		if (string_analizar[i] == 32) {
			espacios++;
		}
	}

	palabrasTDA **palabrasArray = malloc(sizeof(palabrasTDA*) * (espacios + 10));
	memset(palabrasArray, 0, sizeof(palabrasTDA**));

	int pid = fork();

    if ( pid == -1 ) {
        perror("fork() failed");
        exit(EXIT_FAILURE);
    } else if (pid == 0) {
    		// PROCESOS DE ANALISIS
		for (int i = 0; i <= strlen(string_analizar); ++i) {
			if (string_analizar[i] == 32 || strlen(string_analizar) == i) {
				// printf("%s\n", palabraTmp);
				indexPalabraTmp = 0;
				generar(palabrasArray, indexArray, palabraTmp);
				palabraTmp = malloc(sizeof(50));
				indexArray++;
				continue;
			}
			palabraTmp[indexPalabraTmp] = string_analizar[i];
			indexPalabraTmp++;
		}
		int cont = 0;
		palabrasTDA *ch = palabrasArray[0];
		do {
			ch = palabrasArray[cont];
			cont++;
			if (ch != NULL) {
				fprintf (fp, "%s %d\n", ch->palabra, ch->cantidad);
				// printf("palabra: %s, cantidad: %d\n", ch->palabra, ch->cantidad);			
			}
		} while (ch != NULL);
		return(0);
		// PROCESOS DE ANALISIS
    } else {
    	int status;
    	if ( waitpid(pid, &status, 0) != -1 ) {
    		if ( WIFEXITED(status) ) {
    			// printf("Padre (%d, hijo de %d)\n", getpid(), getppid());
    			int returned = WEXITSTATUS(status);
                printf("SALIDA NORMAL CON EL CODIGO: %d\n", returned);
    		} else if ( WIFSIGNALED(status) ) {
    			int signum = WTERMSIG(status);
                printf("Exited due to receiving signal %d\n", signum);
    		} else if  ( WIFSTOPPED(status) )  {
    			int signum = WSTOPSIG(status);
                printf("Stopped due to receiving signal %d\n", signum);
    		} else {
    			printf("Something strange just happened.\n");
    		}
    	} else {
    		perror("FAILED");
            exit(EXIT_FAILURE);
    	}
    }
    return(0);
}